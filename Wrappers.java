public class Wrappers {

    public static void main (String[] args) {

        Byte byteW = 120;
        Short shortW = 1;
        Integer intW = 1;
        Long longW = 10L;
        Float floatW = 10F;
        Double doubleW = 10D;
        Character charW = 'C';
        Boolean booleanW = true;


        System.out.println("Valor Wrapper Byte: " +byteW);
        System.out.println("Valor Wrapper Short: " +shortW);
        System.out.println("Valor Wrapper Integer: " +intW);
        System.out.println("Valor Wrapper Long: " +longW);
        System.out.println("Valor Wrapper Float: " +floatW);
        System.out.println("Valor Wrapper Double: " +doubleW);
        System.out.println("Valor Wrapper Caracter: " +charW);
        System.out.println("Valor Wrapper Boolean: " +booleanW);




    }
    
}
